Набор компонент для платежной системы РобоКасса
===


### Настройка

Необходимые настройки приложения (configs/main.php):

~~~
<?php
// Добавим пседноним для пути, чтоб правильно работали `namespace`
'aliases' => array(
	'Robokassa' => 'ext.yii-robokassa',
),
// Добавим контроллер, действия этого контроллера нужно сохранить в системе РОБОКАССЫ
// ResultURL - /robokassa/result, SuccessURL - /robokassa/success, FailureURL - /robokassa/failure
'controllerMap' => array(
	'robokassa' =>array(
		'class' => '\Robokassa\Controllers\DefaultController',
	),
),
// Добавляем компонент с настройками системы
'components' => array(
	'robokassa' => array(
		'class' => '\Robokassa\Components\Robokassa',
		'invoiceClassName' => 'Invoice',
		'merchantLogin' => 'demo',
	),
),
~~~

### Регистрация магазина

Для того чтобы РОБОКАССА принимала платежи, сначала нужно зарегестриваться.

При регисстрации потребуются следующие данные:

- `RoboKassa::$merchantLogin` - логин в системе РОБОКАССА (sMerchantLogin)
- `RoboKassa::$merchantPass1` - используется интерфейсом инициализации оплаты (sMerchantPass1);
- `RoboKassa::$merchantPass2` - используется интерфейсом оповещения о платеже, XML-интерфейсах (sMerchantPass2);
- `RoboKassa::$resultMethod` - метод отсылки данных в Result URL, по умолчанию `post`
- `RoboKassa::$successMethod` - метод отсылки данных в Success URL, по умолчанию `post`
- `RoboKassa::$failureMethod` - метод отсылки данных в Fail URL, по умолчанию `post`
- `DefaultController::actionNotify()` - используется интерфейсом оповещения о платеже (ResultURL)
- `DefaultController::actionSuccess()` - используется в случае успешного проведения платежа (SuccessURL)
- `DefaultController::actionFailure()` - используется в случае отказа проведения платежа (FailURL)


### Компоненты

- `Robokassa\Interface\Invoice` - интерфейс фактуры
- `Robokassa\Components\Robokassa` - компонент приложения
- `Robokassa\Controllers\DefaultController` - стандартный контроллер для общения с РОБОКАССОЙ
- `Robokassa\Models\Transaction` - модель транзакции
- `Robokassa\Widgets\RobokassaWidget` - виджет РОБОКААССЫ для платежей
- `Robokassa\Widgets\PaymentForm` - виджет генерирующий форму инициализации оплаты


### Xml-интрефейсы

- `CurrenciesList`
- `OperationStateResponse`
- `PaymentMethodsList`
- `RatesList`


### Использование

Для работы системы нужно иметь модель фактуры.

Имя класса этой модель мы должны установить в компонент `\Robokassa\Robokassa::$invoiceClassName`

Это может быть любая модель вашего прилодения `ActiveRecord` или `Model`,

но `implements Robokassa\Interfaces\Invoice`,

например создадим модель `models\Invoice.php`

~~~
<?php
class Invoice extends CModel implements Robokassa\Interfaces\Invoice
{
	/** @var integer */
	public $id = 0;
	/** @var float */
	public $amount = 10.25;
	/** @var string */
	public $description = '';
	/** @var string */
	public $userCurrency = '';
	/** @var string */
	public $userMail = '';
	/**
	 * @var mixed The additional param
	 * @see RoboKassa::$params
	 */
	public $shpClientId = 1;

	/** @return int */
	public function getInvoiceId()
	{
		return $this->id;
	}

	/** @return float */
	public function getAmount()
	{
		return $this->amount;
	}

	/** @return string */
	public function getDescription()
	{
		return $this->description;
	}

	/** @return string */
	public function getUserCurrency()
	{
		return $this->userCurrency;
	}

	/** @return string */
	public function getUserMail()
	{
		return $this->userMail;
	}

	/** Payment complete */
	public function completePayment()
	{
		return true;
	}

	/** User confirm payment */
	public function confirmPayment()
	{
		return true;
	}

	/** User cancel payment */
	public function cancelPayment()
	{
		return true;
	}

	/**
	 * @param integer $invoiceId
	 * @return self
	 */
	public function findByInvoiceId($invoiceId)
	{
		return $this;
	}
}
~~~

###### Примерное дейстие контроллера при покупке:

~~~
<?php
class PaymentController extends Controller
	public function actionIndex()
	{
		/**
		 * Создадим или используем уже существующий в базе инвойс
		 * @var Invoice $invoice
		 */
		$invoice = new Invoice;
		$invoice->id = 1;
		$invoice->amount = 10.25;
		$invoice->description = 'Test payment';
		// Дополнительные параметры
		$invoice->shpClientId = 1;

		// Создаем транзакцию, обратите внимание, конструктор принимает два параметра:
		// первый это сценарий, второй это экземпляр компонента Robokassa
		$transaction = new Transaction(Transaction::SCENARIO_CREATE, $this->robokassa);
		// Добавляем инвойс к оплате
		$transaction->chargeInvoice($invoice);

		// Теперь у нас есть готовая транзакцию, которую можно отправить в РОБОКАССУ
		$url = Robokassa::URL_TEST . '?' . Robokassa::createQueryString($transaction);
		Yii::app()->controller->redirect($url);
	}
}
~~~

###### Испольование виджетов

~~~
<?php
class PaymentController extends Controller
	public function actionIndex()
	{
		/**
		 * Создадим или используем уже существующий в базе инвойс
		 * @var Invoice $invoice
		 */
		$invoice = new Invoice;
		$invoice->id = 1;
		$invoice->amount = 10.25;
		$invoice->description = 'Test payment';
		// Дополнительные параметры
		$invoice->shpClientId = 1;

		// Показываем форму оплаты
		$this->render('index', array(
			'invoice' => $invoice,
		));
	}
}
~~~

представление `/views/payment/index.php`

~~~
<?php
echo CHtml::openTag('div');

// Ресуем данные платежа
echo CHtml::tag('p', array(), 'В вашей корзине X товаров');
echo CHtml::tag('p', array(), 'На сумму: ' . $invoice->amount);
echo CHtml::tag('p', array(), 'Будем платить?');

// Кнопка отправки формы и сама форма
$this->widget('\Robokassa\Widgets\PaymentForm', array(
	'baseUrl' => Robokassa::URL_TEST,
	'invoice' => $invoice,
));

echo CHtml::closeTag('div');
~~~


Всё.