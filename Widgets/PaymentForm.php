<?php
/**
 * \Robokassa\Widgets\PaymentForm class file.
 *
 * @author Veaceslav Medvedev <slavcopost@gmail.com>
 * @license http://www.opensource.org/licenses/bsd-license.php
 * @link https://github.com/yiiext
 * @version 0.1
 */

namespace Robokassa\Widgets;

use Yii,
	CHtml as Html,
	Robokassa\Components\Robokassa;

/**
 * Simple form.
 */
class PaymentForm extends RobokassaWidget
{
	/** @var string */
	public $baseUrl = Robokassa::URL_BASE;
	/** @var string */
	public $method = 'post';
	/** @var array */
	public $htmlOptions = array();
	/** @var array */
	public $buttonHtmlOptions = array();

	/**
	 * Render the form.
	 */
	public function renderForm()
	{
		echo Html::beginForm($this->baseUrl, $this->method, $this->htmlOptions);
		$this->renderElements();
		echo Html::submitButton(Yii::t('payments', 'Pay'), $this->buttonHtmlOptions);
		echo Html::endForm();
	}

	/**
	 * Render ROBOKASSA hidden fields.
	 */
	public function renderElements()
	{
		foreach ($this->transaction->attributeLabels() as $name => $key) {
			echo Html::hiddenField($key, $this->transaction->$name);
		}
	}
}