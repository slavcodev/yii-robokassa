<?php
/**
 * \Robokassa\XmlData\PaymentMethodsList class file.
 *
 * @author Veaceslav Medvedev <slavcopost@gmail.com>
 * @license http://www.opensource.org/licenses/bsd-license.php
 * @link https://github.com/yiiext
 * @version 0.1
 */

namespace Robokassa\Models\Xml;

/**
 * Список способов оплаты, доступных покупателям данного магазина
 *
 * ```
 * 	<?xml version="1.0" encoding="utf-8" ?>
 * 	<PaymentMethodsList xmlns="http://merchant.roboxchange.com/WebService/">
 * 		<Result>
 * 			<Code>integer</Code>
 * 			<Description>string</Description>
 * 		</Result>
 * 		<Methods>
 * 			<Method Code="string" Description="string" />
 * 			<Method Code="string" Description="string" />
 * 			...
 * 		</Methods>
 * 	</PaymentMethodsList>
 * ```
 */
class PaymentMethodsList extends Model
{
	/** @var string */
	private $_login;
	/** @var string */
	private $_language = '';

	/**
	 * @param string $login
	 * @param string $language
	 */
	function __construct($login, $language = '')
	{
		$this->_login = $login;
		$this->_language = $language;
	}

	public function methodName()
	{
		return 'GetPaymentMethods';
	}

	public function params()
	{
		return array(
			'MerchantLogin' => $this->_login,
			'Language' => $this->_language,
		);
	}
}