<?php
/**
 * \Robokassa\XmlData\RatesList class file.
 *
 * @author Veaceslav Medvedev <slavcopost@gmail.com>
 * @license http://www.opensource.org/licenses/bsd-license.php
 * @link https://github.com/yiiext
 * @version 0.1
 */

namespace Robokassa\Models\Xml;

/**
 * Позволяет рассчитать сумму, которую должен будет заплатить клиент
 *
 * ```
 * 	<?xml version="1.0" encoding="utf-8" ?>
 * 	<RatesList xmlns="http://merchant.roboxchange.com/WebService/">
 * 		<Result>
 * 			<Code>integer</Code>
 * 			<Description>string</Description>
 * 		</Result>
 * 		<Groups>
 * 			<Group Code="string" Description="string">
 * 				<Items>
 * 					<Currency Label="string" Name="string">
 * 						<Rate IncSum="decimal" />
 * 					</Currency>
 * 					...
 * 				</Items>
 * 			</Group>
 * 			...
 * 		</Groups>
 * 	</RatesList>
 * ```
 */
class RatesList extends Model
{
	/** @var string */
	private $_login;
	/** @var string */
	private $_language = '';
	/** @var string */
	private $_currency = '';
	/** @var float */
	private $_amount;

	/**
	 * @param string $login
	 * @param string $currency
	 * @param float $amount
	 * @param string $language
	 */
	function __construct($login, $amount, $currency = '', $language = '')
	{
		$this->_login = $login;
		$this->_currency = $currency;
		$this->_amount = $amount;
		$this->_language = $language;
	}

	public function methodName()
	{
		return 'GetRates';
	}

	public function params()
	{
		return array(
			'MerchantLogin' => $this->_login,
			'IncCurrLabel' => $this->_currency,
			'OutSum' => $this->_amount,
			'Language' => $this->_language,
		);
	}
}