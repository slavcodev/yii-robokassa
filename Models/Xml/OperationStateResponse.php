<?php
/**
 * \Robokassa\XmlData\OperationStateResponse class file.
 *
 * @author Veaceslav Medvedev <slavcopost@gmail.com>
 * @license http://www.opensource.org/licenses/bsd-license.php
 * @link https://github.com/yiiext
 * @version 0.1
 */

namespace Robokassa\Models\Xml;

/**
 * Возвращает детальную информацию о текущем состоянии и реквизитах оплаты.
 *
 * Необходимо помнить, что операция инициируется не в момент ухода пользователя на оплату,
 * а позже - после подтверждения его платежных реквизитов,
 * т.е. вы вполне можете не находить операцию, которая по вашему мнению уже должна начаться.
 *
 * ```
 * 	<?xml version="1.0" encoding="utf-8" ?>
 * 	<OperationStateResponse xmlns="http://merchant.roboxchange.com/WebService/">
 * 		<Result>
 * 			<Code>integer</Code>
 * 			<Description>string</Description>
 * 		</Result>
 * 		<State>
 * 			<Code>integer</Code>
 * 			<RequestDate>datetime</RequestDate>
 * 			<StateDate>datetime</StateDate>
 * 		</State>
 * 		<Info>
 * 			<IncCurrLabel>string</IncCurrLabel>
 * 			<IncSum>decimal</IncSum>
 * 			<IncAccount>string</IncAccount>
 * 			<PaymentMethod>
 * 				<Code>string</Code>
 * 				<Description>string</Description>
 * 			</PaymentMethod>
 * 			<OutCurrLabel>string</OutCurrLabel>
 * 			<OutSum>decimal</OutSum>
 * 		</Info>
 * 	</OperationStateResponse>
 * ```
 */
class OperationStateResponse extends Model
{
	/** @var string */
	private $_login;
	/** @var integer */
	private $_invoiceId;
	/** @var string */
	private $_signature;

	/**
	 * @param string $login
	 * @param string $invoiceId
	 * @param string $signature
	 */
	function __construct($login, $invoiceId, $signature)
	{
		$this->_login = $login;
		$this->_invoiceId = $invoiceId;
		$this->_signature = $signature;
	}

	public function methodName()
	{
		return 'OpState';
	}

	public function params()
	{
		return array(
			'MerchantLogin' => $this->_login,
			'InvoiceID' => $this->_invoiceId,
			'Signature' => $this->_signature,
		);
	}
}