<?php
/**
 * \Robokassa\Components\Robokassa class file.
 *
 * @author Veaceslav Medvedev <slavcopost@gmail.com>
 * @license http://www.opensource.org/licenses/bsd-license.php
 * @link https://github.com/yiiext
 * @version 0.1
 */

namespace Robokassa\Components;

use Yii,
	CException as Exception,
	CApplicationComponent as Component,
	CHtml as Html,
	Robokassa\Models\Transaction;

/**
 * Зарегестрированный в РОБОКАССЕ магазин.
 *
 * @property string $merchantLogin The merchant login.
 * @property string $merchantPass1 The payment password.
 * @property string $merchantPass2 The notify password.
 * @property string $resultMethod
 * @property string $successMethod
 * @property string $failureMethod
 * @property array $params
 *
 * @version 0.1
 * @link http://robokassa.ru/
 * @see http://robokassa.ru/ru/Doc/Ru/Interface.aspx
 */
class Robokassa extends Component
{
	const URL_TEST		= 'http://test.robokassa.ru/Index.aspx';
	const URL_BASE		= 'https://merchant.roboxchange.com/Index.aspx';
	const PARAM_PREFIX	= 'shp';
	const METHOD_GET	= 'get';
	const METHOD_POST	= 'post';

	/** @var string */
	public $currency = '$';

	/** @var string */
	private $_merchantLogin = 'demo';
	/** @var string */
	private $_merchantPass1 = 'password_1';
	/** @var string */
	private $_merchantPass2 = 'password_2';
	/** @var string */
	private $_resultMethod = self::METHOD_POST;
	/** @var string */
	private $_successMethod = self::METHOD_POST;
	/** @var string */
	private $_failureMethod = self::METHOD_POST;
	/** @var array */
	private $_params = array();
	/** @var string */
	private $_invoiceClassName;

	/**
	 * @param Transaction $transaction
	 * @return string
	 */
	public static function createQueryString(Transaction $transaction)
	{
		$url = '';
		foreach ($transaction->attributeLabels() as $name => $key) {
			$url .= $key . '=' . urlencode($transaction->$name) . '&';
		}
		return rtrim($url, '&');
	}

	/**
	 * @param string $invoiceClassName
	 */
	public function setInvoiceClassName($invoiceClassName)
	{
		$this->_invoiceClassName = (string) $invoiceClassName;
	}

	/**
	 * @return string
	 * @throws Exception
	 */
	public function getInvoiceClassName()
	{
		if (empty($this->_invoiceClassName)) {
			throw new Exception('Invalid invoice model class name.');
		}
		return $this->_invoiceClassName;
	}

	/**
	 * @param array $params
	 */
	public function setParams(array $params)
	{
		$checkPrefix = function(&$name, $index, $prefix) {
			if ($prefix !== substr($name, 0, strlen($prefix))) {
				$name = $prefix . $name;
			}
			return $index;
		};

		array_walk($params, $checkPrefix, RoboKassa::PARAM_PREFIX);
		ksort($params);
		$this->_params = $params;
	}

	/**
	 * @return array
	 */
	public function getParams()
	{
		return $this->_params;
	}

	/**
	 * @param string $merchantLogin
	 */
	public function setMerchantLogin($merchantLogin = '')
	{
		$this->_merchantLogin = (string) $merchantLogin;
	}

	/**
	 * @return string
	 * @throws Exception
	 */
	public function getMerchantLogin()
	{
		if (null === $this->_merchantLogin) {
			throw new Exception('Invalid ROBOKASSA login.');
		}
		return $this->_merchantLogin;
	}

	/**
	 * @param string $merchantPass1
	 */
	public function setMerchantPass1($merchantPass1 = '')
	{
		$this->_merchantPass1 = (string) $merchantPass1;
	}

	/**
	 * @return string
	 * @throws Exception
	 */
	public function getMerchantPass1()
	{
		if (null === $this->_merchantPass1) {
			throw new Exception('Invalid ROBOKASSA pass #1.');
		}
		return $this->_merchantPass1;
	}

	/**
	 * @param string $merchantPass2
	 */
	public function setMerchantPass2($merchantPass2 = '')
	{
		$this->_merchantPass2 = (string) $merchantPass2;
	}

	/**
	 * @return string
	 * @throws Exception
	 */
	public function getMerchantPass2()
	{
		if (null === $this->_merchantPass2) {
			throw new Exception('Invalid ROBOKASSA pass #2.');
		}
		return $this->_merchantPass2;
	}

	/**
	 * @param string $failureMethod
	 */
	public function setFailureMethod($failureMethod = self::METHOD_POST)
	{
		$this->_failureMethod = strtolower($failureMethod) === self::METHOD_GET ? self::METHOD_GET : self::METHOD_POST;
	}

	/**
	 * @return string
	 */
	public function getFailureMethod()
	{
		return $this->_failureMethod;
	}

	/**
	 * @param string $resultMethod
	 */
	public function setResultMethod($resultMethod = self::METHOD_POST)
	{
		$this->_resultMethod = strtolower($resultMethod) === self::METHOD_GET ? self::METHOD_GET : self::METHOD_POST;
	}

	/**
	 * @return string
	 */
	public function getResultMethod()
	{
		return $this->_resultMethod;
	}

	/**
	 * @param string $successMethod
	 */
	public function setSuccessMethod($successMethod = self::METHOD_POST)
	{
		$this->_successMethod = strtolower($successMethod) === self::METHOD_GET ? self::METHOD_GET : self::METHOD_POST;
	}

	/**
	 * @return string
	 */
	public function getSuccessMethod()
	{
		return $this->_successMethod;
	}
}