<?php
/**
 * \Robokassa\Test\Invoice class file.
 *
 * @author Veaceslav Medvedev <slavcopost@gmail.com>
 * @license http://www.opensource.org/licenses/bsd-license.php
 * @link https://github.com/yiiext
 * @version 0.1
 */

namespace Robokassa\Test;

use Yii,
	YiiPlus\Db\Model,
	Robokassa\Interfaces;

/**
 * Payment transaction model.
 */
class Invoice extends Model implements Interfaces\Invoice
{
	/** @var integer */
	public $id = 1;
	/** @var float */
	public $amount = 100.01;
	/** @var string */
	public $description = 'Test payment';
	/** @var string */
	public $userCurrency = 'WMZ';
	/** @var string */
	public $userMail = '';
	/**
	 * @var mixed The additional param
	 * @see RoboKassa::$params
	 */
	public $shpItem1 = '1 Database';
	public $shpItem2 = '5GB Storage';
	public $shpItem3 = '20 Users';

	/**
	 * @return int
	 */
	public function getInvoiceId()
	{
		return $this->id;
	}

	/**
	 * @return float
	 */
	public function getAmount()
	{
		return $this->amount;
	}

	/**
	 * @return string
	 */
	public function getDescription()
	{
		return $this->description;
	}

	/**
	 * @return string
	 */
	public function getUserCurrency()
	{
		return $this->userCurrency;
	}

	/**
	 * @return string
	 */
	public function getUserMail()
	{
		return $this->userMail;
	}

	/** Payment complete */
	public function completePayment()
	{
		return true;
	}

	/** User confirm payment */
	public function confirmPayment()
	{
		return true;
	}

	/** User cancel payment */
	public function cancelPayment()
	{
		return true;
	}

	/**
	 * TODO: Implements method.
	 * @param integer $invoiceId
	 * @return self
	 */
	public function findByInvoiceId($invoiceId)
	{
		$this->id = $invoiceId;
		return $this;
	}
}