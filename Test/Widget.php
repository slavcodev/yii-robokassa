<?php
/**
 * \Robokassa\Test\Widget class file.
 *
 * @author Veaceslav Medvedev <slavcopost@gmail.com>
 * @license http://www.opensource.org/licenses/bsd-license.php
 * @link https://github.com/yiiext
 * @version 0.1
 */

namespace Robokassa\Test;

use Yii,
	CHtml as Html,
	Robokassa\Components\Robokassa,
	Robokassa\Interfaces,
	Robokassa\Widgets\PaymentForm;

/**
 * Payment transaction model.
 */
class Widget extends PaymentForm
{
	/**
	 * Render the form.
	 */
	public function renderForm()
	{
		echo Html::openTag('ul', array('class' => 'pricing-table'));

		echo Html::tag('li', array('class' => 'title'), 'Standard');
		echo Html::tag('li', array('class' => 'price'), $this->invoice->getAmount());
		echo Html::tag('li', array('class' => 'description'), $this->invoice->getDescription());

		foreach ($this->robokassa->params as $param) {
			echo Html::tag('li', array('class' => 'bullet-item'), $this->invoice->$param);
		}

		echo Html::openTag('li', array('class' => 'cta-button'));
		$this->buttonHtmlOptions['class'] = 'button';
		$this->buttonHtmlOptions['label'] = 'Buy it Now &raquo;';
		parent::renderForm();
		echo Html::closeTag('li');

		echo Html::closeTag('ul');
	}
}