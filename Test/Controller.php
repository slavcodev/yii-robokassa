<?php
/**
 * \Robokassa\Test\Controller class file.
 *
 * @author Veaceslav Medvedev <slavcopost@gmail.com>
 * @license http://www.opensource.org/licenses/bsd-license.php
 * @link https://github.com/yiiext
 * @version 0.1
 */

namespace Robokassa\Test;

use Yii,
	YiiPlus\VarDumper as Dumper,
	Robokassa\Components\Robokassa,
	Robokassa\Controllers\DefaultController,
	Robokassa\Models\Transaction,
	Robokassa\Models\Xml;

class Controller extends DefaultController
{
	/**
	 * @return Invoice
	 */
	public function createInvoice()
	{
		/** @var Invoice $invoice  */
		$invoice = parent::createInvoice();
		return $invoice;
	}

	public function actionReadme()
	{
		$md = new \CMarkdown();
		$this->renderText($md->transform(file_get_contents(Yii::getPathOfAlias('Robokassa') . '/readme.md')));
	}

	public function actionIndex()
	{
		$this->pageTitle = 'Payment URL';
		$invoice = $this->createInvoice();

		$transaction = new Transaction(Transaction::SCENARIO_CREATE, $this->robokassa);
		$transaction->chargeInvoice($invoice);

		$url = Robokassa::URL_TEST . '?' . Robokassa::createQueryString($transaction);
		$cap = \CHtml::tag('p', array('class' => 'panel',), $url);

		$cap.= \CHtml::openTag('div', array('class' => 'three column'));
		$cap.= $this->widget('\Robokassa\Test\Widget', array(
				'baseUrl' => Robokassa::URL_TEST,
				'invoice' => $this->createInvoice(),
			), true);
		$cap.= \CHtml::closeTag('div');

		$cap.= \CHtml::openTag('div', array('class' => 'six last column'));
		$cap.= $this->widget('\Robokassa\Widgets\RobokassaWidget', array(
				'invoice' => $this->createInvoice(),
			), true);
		$cap.= \CHtml::closeTag('div');

		$this->renderText($cap);
	}

	public function actionXml()
	{
		$data = new Xml\CurrenciesList('demo', 'ru');
		$data->loadXml();
		Dumper::renderDump($data);
		//$data = new Xml\RatesList('demo', 10, 'YandexMerchantR');
		//\YiiPlus\VarDumper::dump($data->xml);
	}
}