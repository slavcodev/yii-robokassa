<?php
/**
 * \Robokassa\Interfaces\Invoice class file.
 *
 * @author Veaceslav Medvedev <slavcopost@gmail.com>
 * @license http://www.opensource.org/licenses/bsd-license.php
 * @link https://github.com/yiiext
 * @version 0.1
 */

namespace Robokassa\Interfaces;

/**
 * Payment invoice interface.
 */
interface Invoice
{
	/**
	 * @return integer
	 */
	public function getInvoiceId();

	/**
	 * @return float
	 */
	public function getAmount();

	/**
	 * @return string
	 */
	public function getDescription();

	/**
	 * @return string
	 */
	public function getUserCurrency();

	/**
	 * @return string
	 */
	public function getUserMail();

	/**
	 * Payment complete.
	 * @return boolean
	 */
	public function completePayment();

	/**
	 * User confirm payment.
	 * @return boolean
	 */
	public function confirmPayment();

	/**
	 * User cancel payment.
	 * @return boolean
	 */
	public function cancelPayment();

	/**
	 * @param integer $invoiceId
	 * @return self
	 */
	public function findByInvoiceId($invoiceId);
}