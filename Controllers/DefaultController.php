<?php
/**
 * \Robokassa\Controllers\DefaultController class file.
 *
 * @author Veaceslav Medvedev <slavcopost@gmail.com>
 * @license http://www.opensource.org/licenses/bsd-license.php
 * @link https://github.com/yiiext
 * @version 0.1
 */

namespace Robokassa\Controllers;

use Yii,
	YiiPlus\VarDumper as Dumper,
	CException as Exception,
	CHttpException as HttpException,
	CLogger as Logger,
	Controller as BaseController,
	Robokassa\Interfaces,
	Robokassa\Models\Transaction,
	Robokassa\Components\Robokassa;

/**
 * Result request:
 * ```
 * OutSum=$nOutSum&InvId=$nInvId&
 * SignatureValue=$sSignatureValue=nOutSum:nInvId:sMerchantPass2[:пользовательские параметры, в отсортированном порядке]
 * [&пользовательские_параметры]
 * ```
 *
 * Answer format:
 * ```
 * OK$nInvId
 * ```
 *
 * Success request:
 * ```
 * OutSum=$nOutSum&InvId=$nInvId&Culture=$sCulture&
 * SignatureValue=$sSignatureValue=nOutSum:nInvId:sMerchantPass1[:пользовательские параметры, в отсортированном порядке]
 * [&пользовательские_параметры]
 * ```
 *
 * Failure request:
 * ```
 * OutSum=$nOutSum&InvId=$nInvId&Culture=$sCulture[&пользовательские_параметры]
 * ```
 *
 * @property RoboKassa $robokassa
 *
 * @link http://robokassa.ru/
 * @see http://robokassa.ru/ru/Doc/Ru/Interface.aspx
 */
class DefaultController extends BaseController
{
	/** @var string */
	public $componentId = 'robokassa';

	/**
	 * Enable post-only filter for actions.
	 * @return array
	 */
	public function filters()
	{
		$actions = array();
		foreach (array('result', 'success', 'failure',) as $action) {
			$method = $action . 'Method';
			if ($this->robokassa->$method === RoboKassa::METHOD_POST) {
				$actions[] = $action;
			}
		}

		if (count($actions) > 0) {
			return array('postOnly + ' . implode(', ', $actions));
		} else {
			return array();
		}
	}

	/**
	 * @return Robokassa
	 */
	public function getRobokassa()
	{
		return Yii::app()->getComponent($this->componentId);
	}

	/**
	 * @return Interfaces\Invoice
	 * @throws Exception
	 */
	public function createInvoice()
	{
		$className = $this->robokassa->invoiceClassName;
		return new $className;
	}

	public function actionResult()
	{
		$transaction = new Transaction(Transaction::SCENARIO_RESULT, $this->robokassa);
		$transaction->loadData($this->robokassa->resultMethod === Robokassa::METHOD_GET ? $_GET : $_POST);

		if ($transaction->validate() && $transaction->invoice->completePayment()) {
			Yii::log("Payment #{$transaction->invoiceId} complete", Logger::LEVEL_INFO, $this->componentId);
			echo "OK{$transaction->invoiceId}\n";
		} else {
			Yii::log("Payment #{$transaction->invoiceId} confirmation error", Logger::LEVEL_INFO, $this->componentId);
			Yii::log(Dumper::dumpAsString($transaction->errors, 10, false), Logger::LEVEL_ERROR, $this->componentId);
			echo "ERR{$transaction->invoiceId}\n";
		}
		Yii::app()->end();
	}

	public function actionSuccess()
	{
		$transaction = new Transaction(Transaction::SCENARIO_SUCCESS, $this->robokassa);
		$transaction->loadData($this->robokassa->successMethod === Robokassa::METHOD_GET ? $_GET : $_POST);

		if ($transaction->validate() && $transaction->invoice->confirmPayment()) {
			Yii::log("Payment #{$transaction->invoiceId} confirm", Logger::LEVEL_INFO, $this->componentId);
			$amountString = Yii::app()->numberFormatter->formatCurrency($transaction->amount, $this->robokassa->currency);
			$this->renderText("Подтверждена оплата {$amountString}. Заказ #{$transaction->invoiceId} принят.");
		} else {
			Yii::log("Payment #{$transaction->invoiceId} confirmation error", Logger::LEVEL_INFO, $this->componentId);
			Yii::log(Dumper::dumpAsString($transaction->errors, 10, false), Logger::LEVEL_ERROR, $this->componentId);
			throw new HttpException(400, Yii::t('yii', 'Your request is invalid.'));
		}
	}

	public function actionFailure()
	{
		$transaction = new Transaction(Transaction::SCENARIO_FAILURE, $this->robokassa);
		$transaction->loadData($this->robokassa->failureMethod === Robokassa::METHOD_GET ? $_GET : $_POST);

		if ($transaction->validate() && $transaction->invoice->cancelPayment()) {
			Yii::log('Payment failure', Logger::LEVEL_WARNING, $this->componentId);
			$formatter = Yii::app()->numberFormatter;
			$amountString = $formatter->formatCurrency($transaction->amount, $this->robokassa->currency);
			$this->renderText("Вы отказались от оплаты {$amountString}. Заказ #{$transaction->invoiceId} отменен.");
		} else {
			Yii::log("Payment #{$transaction->invoiceId} cancel error", Logger::LEVEL_INFO, $this->componentId);
			Yii::log(Dumper::dumpAsString($transaction->errors, 10, false), Logger::LEVEL_ERROR, $this->componentId);
			throw new HttpException(400, Yii::t('yii', 'Your request is invalid.'));
		}
	}
}